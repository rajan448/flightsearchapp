package com.booking.flight.controller;

import com.booking.flight.model.Flight;
import com.booking.flight.repo.FlightRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FlightController {

    private FlightRepository repo;

    FlightController( FlightRepository flightRepository) {
        this.repo = flightRepository;
    }

    @GetMapping("/flights")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Flight> getAllFlights(@RequestParam(value = "page", required = false) Integer page,
                                      @RequestParam(value = "pageSize", required = false) Integer pageSize) {
        if (page == null) page = 0;
        if (pageSize == null) pageSize = 100;

        return repo.findAll(PageRequest.of(page, pageSize)).getContent();
    }

    @PostMapping("flight/add")
    @CrossOrigin(origins = "http://localhost:4200")
    public Flight insertAFlight(@RequestBody() Flight flight) {
        return repo.save(flight);
    }

    @GetMapping("/demo")
    @CrossOrigin(origins = "http://localhost:4200")
    public void addDemoFlight() {
        // Add One demo flight if db is empty
        if(repo.count() == 0) {
            Flight flight = new Flight("Air India", "Delhi", "Goa", 10000, 100, 120);
            repo.save(flight);
        }
    }
}
