package com.booking.flight.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document
public class Flight {

    @Id
    private String _id;
    private String airline;
    private String sourceCity;
    private String destinationCity;

    private long fare;
    private int availableSeats;
    private int totalSeats;

    public Flight() {
    }

    public Flight(String airline, String sourceCity, String destinationCity,
                  long fare, int availableSeats, int totalSeats) {
        this.airline = airline;
        this.sourceCity = sourceCity;
        this.destinationCity = destinationCity;
        this.fare = fare;
        this.availableSeats = availableSeats;
        this.totalSeats = totalSeats;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getSourceCity() {
        return sourceCity;
    }

    public void setSourceCity(String sourceCity) {
        this.sourceCity = sourceCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public long getFare() {
        return fare;
    }

    public void setFare(long fare) {
        this.fare = fare;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    public int getTotalSeats() {
        return totalSeats;
    }

    public void setTotalSeats(int totalSeats) {
        this.totalSeats = totalSeats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return fare == flight.fare &&
                availableSeats == flight.availableSeats &&
                totalSeats == flight.totalSeats &&
                Objects.equals(_id, flight._id) &&
                Objects.equals(airline, flight.airline) &&
                Objects.equals(sourceCity, flight.sourceCity) &&
                Objects.equals(destinationCity, flight.destinationCity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(_id, airline, sourceCity, destinationCity, fare, availableSeats, totalSeats);
    }

    @Override
    public String toString() {
        return "Flight{" +
                "_id=" + _id +
                ", airline='" + airline + '\'' +
                ", sourceCity='" + sourceCity + '\'' +
                ", destinationCity='" + destinationCity + '\'' +
                ", fare=" + fare +
                ", availableSeats=" + availableSeats +
                ", totalSeats=" + totalSeats +
                '}';
    }
}
